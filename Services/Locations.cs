﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Services
{
    [DataContract]
    public class Locations
    {
        [Key]
        [DataMember]
        public Int32 locID { get; set; }
        [DataMember]
        public String locName { get; set; }
        [DataMember]
        public String userName { get; set; }
        [DataMember]
        public String latitude { get; set; }
        [DataMember]
        public String longitude { get; set; }
        [DataMember]
        public DateTime date { get; set; }
    }
}