﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SignalRMapDemo.Controllers
{
    public class MapController : Controller
    {
        // GET: Map
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Leaflet()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Mobile()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NewMap()
        {
            ViewBag.userName = null;
            return View();
        }

        [HttpPost]
        public ActionResult NewMap(string userName)
        {
            ViewBag.userName = userName;
            return View();
        }
        [HttpGet]
        public ActionResult PureLeaflet()
        {
            ViewBag.userName = null;
            return View();
        }

        [HttpPost]
        public ActionResult PureLeaflet(string userName)
        {
            ViewBag.userName = userName;
            return View();
        }


        [HttpPost]
        public JsonResult Create(string dataz)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //Person[] persons = js.Deserialize<Person[]>(json);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            ServiceReference1.Locations objCoord = sr.Deserialize<ServiceReference1.Locations>(dataz);

            ServiceReference1.Service1Client obj = new ServiceReference1.Service1Client("Service1");
            //obj.SaveData();
             string msg =  obj.SaveData(objCoord);
             return Json("Success ;) " + msg);
        }
    }

    //public class Locations
    //{
        
    //    public Int32 locID { get; set; }
    //    public String locName { get; set; }
    //    public String userName { get; set; }
    //    public String latitude { get; set; }
    //    public String longitude { get; set; }
    //    public DateTime date { get; set; }
    //}
}