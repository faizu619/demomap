﻿var currentLoc;

var map;
var mymarker;
var options = {
    enableHighAccuracy: false,
    timeout: 500,
    maximumAge: 5000
};
var markers = [];
var myHub = jQuery.connection.getRoutes;

var polylineCoord;
var polyline;

var lat;
var lng;


if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(currentPosition, error_callback);
    //navigator.geolocation.watchPosition(locUpdates, error_callback, options)

    setInterval(function () {

        navigator.geolocation.getCurrentPosition(locUpdates, error_callback);

    }, '5000');
}
//jQuery('#displayname').val(prompt('Enter your name:', ''));

myHub.client.GetCoord = function (a, user) {
    var abc = '';
    for (var i = 0; i < markers.length; i++) {
        abc += '<option value="' + markers[i].id + '">' + markers[i].id + '</option>';
    }
    jQuery('#ddlUsers').html(abc);

    for (var i = 0; i < markers.length; i++) {
        if (user == markers[i].id) {
            var oldMark = markers[i].markerObject;
            oldMark.setLatLng(L.latLng(a));
            return;
        }
    }
    var mymarker2 = L.marker(L.latLng(a)).addTo(map);

    mymarker2.bindPopup("<b>Hello, Me " + user + " !</b><br>I am Here.").openPopup();

    markers.push({ id: user, markerObject: mymarker2 });

    //var abc = '';
    //for (var i = 0; i < markers.length; i++) {
    //    abc += '<option value="' + markers[i].markerObject.getLatLng() + '">' + markers[i].id + '</option>';
    //}
    //jQuery('#ddlUsers').html(abc);

    console.log("rec", a);
};

function currentPosition(pos) {

    lat = pos.coords.latitude;
    lng = pos.coords.longitude;

    currentLoc = L.latLng(pos.coords.latitude, pos.coords.longitude); //[pos.coords.latitude, pos.coords.longitude];

    map = L.map('mapid').setView(currentLoc, 16);

    console.log(map);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'SHAHEEN',
        maxZoom: 18,
        id: 'faizu619.pdp6cle7',
        accessToken: 'pk.eyJ1IjoiZmFpenU2MTkiLCJhIjoiY2lsdDViem5oMDA4Y3Vza3N0NWU4bHk3cCJ9.Fgy-HRATWonWZc2gAIoRbw'
    }).addTo(map);

    mymarker = L.marker(currentLoc).addTo(map);

    mymarker.bindPopup("<b>Hello, Me " + jQuery('#displayname').val() + " !</b><br>I am Here.").openPopup();

    markers.push({ id: jQuery('#displayname').val(), markerObject: mymarker });
    sendingCoord(currentLoc);


    polyline = L.polyline(currentLoc, { color: 'red' }).addTo(map);
}

function sendingCoord(a) {
    //console.log(jQuery.connection);
    jQuery.connection.hub.start().done(function () {

        myHub.server.sendCoord(a, jQuery('#displayname').val());
        console.log("send", a);
    });
}


function locUpdates(pos) {

    if (lat == pos.coords.latitude && lng == pos.coords.longitude) { return; }

    currentLoc = L.latLng(pos.coords.latitude, pos.coords.longitude);

    lat = pos.coords.latitude;
    lng = pos.coords.longitude;

    sendingCoord(currentLoc);

    console.log(lat);
    console.log(lng);
    
    polyline.addLatLng(currentLoc);

}
function error_callback(error) {
    document.getElementById("errorcode").innerHTML = error.code;
    document.getElementById("errormsg").innerHTML = error.message;
}

jQuery('#btnSetCentre').click(function () {
    console.log(jQuery('#ddlUsers').val());
    for (var i = 0; i < markers.length; i++) {
        if (jQuery('#ddlUsers').val() == markers[i].id) {
            var oldMark = markers[i].markerObject.getLatLng();
            console.log(jQuery('#ddlUsers').val(), oldMark);
            map.setView(oldMark, 17);
            return;
        }
    }

});